FROM nvidia/cuda:10.0-cudnn7-devel-ubuntu18.04

# build and install cmake
RUN apt update && apt install wget --yes && \
    mkdir build && cd build && \
    wget https://github.com/Kitware/CMake/releases/download/v3.17.3/cmake-3.17.3.tar.gz -O cmake.tar.gz && \
    tar -xvf cmake.tar.gz &&  rm -rf cmake.tar.gz && mv cmake-3.17.3 cmake

RUN cd build/cmake &&  apt install libssl-dev --yes && ./bootstrap
RUN cd build/cmake &&  make -j 2 && make install 

# clone opencv 4.3.0
RUN wget https://github.com/opencv/opencv/archive/4.3.0.tar.gz -O opencv.tar.gz && tar -xvf opencv.tar.gz && mv opencv-4.3.0 opencv && rm -rf opencv.tar.gz

# build opencv
RUN apt install -y libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
RUN mkdir opencv_build && cd opencv_build && \
    cmake -DWITH_FFMPEG=OFF -DWITH_OPENEXR=OFF \
          -DBUILD_TIFF=OFF -DWITH_CUDA=OFF \
          -DWITH_NVCUVID=OFF -DBUILD_PNG=OFF \
          -DBUILD_DOCS=OFF -DBUILD_EXAMPLES=OFF \        
          -DBUILD_opencv_ml=OFF -DBUILD_opencv_dnn=OFF \        
          -DBUILD_TESTS=OFF \
        ../opencv && \
    make -j 2 && make install
# install opencl
RUN apt-get update && apt-get install -y --no-install-recommends \
        ocl-icd-opencl-dev && \
    rm -rf /var/lib/apt/lists/*