## Лабораторная работа по АВП №8

Работа с кластером из множества клиентов

Клиенты осуществляют обработку изображений с помощью операции свертки. Клиенты имеют различную реалиазацию, в частности:
- С использованием библиотеки cuDNN
- На базе расширения языка С CUDA
- С помощью фреймворка OpenCL

Для взаимодействия клиентов и серверов используется **MPI**

Сборка:
```bash
export CUDA_HOME="/usr/lib/cuda"
export CUDNN_PATH="/usr/lib/cuda"
mkdir build
cd build
cmake .. -DBUILD_CUDNN=True -DBUILD_OPENCL=True -DBUILD_CUDA=True -DBUILD_SERVER=True
make -j 4
```

Зависимости:
- CUDA 10.0
- cuDNN 7.4
- OpenCV 4.3

