#pragma once
#include <opencv2/opencv.hpp>

class AbstractExecutor{

public:
    AbstractExecutor() {}

    virtual cv::Mat process(const cv::Mat &image) = 0;

};