#pragma once
#include <ocl_utils.hpp>
#include <vector>
#include <ContextExecutor.hpp>

void CL_CALLBACK contextCallback(const char * errInfo, const void * private_info, size_t cb, void * user_data)
{
  std::cout << "Error occurred during context use: " << errInfo << std::endl;
  exit(EXIT_FAILURE);
}


class ContextFactory
{
public:
    ContextFactory() {}

    void init_contexts()
    {
        cl_int errNum;
        cl_uint numPlatforms;
        cl_platform_id * platformIds;
        std::vector<cl_context> contexts;
        std::vector<cl_device_id*> device_ids;
        cl_uint numDevices;

        errNum = clGetPlatformIDs(0, NULL, &numPlatforms);
        if (errNum != CL_SUCCESS || numPlatforms <= 0)  
            throw std::runtime_error("Failed to find any OpenCL platform.");
        
        
        platformIds = (cl_platform_id *)alloca(sizeof(cl_platform_id) * numPlatforms);
        
        errNum = clGetPlatformIDs(numPlatforms, platformIds, NULL);
        if (errNum != CL_SUCCESS)
            throw std::runtime_error("Failed to find any OpenCL platforms.");
            
        std::cout << "Number of platforms: \t" << numPlatforms << std::endl;
        cl_platform_id *platformIDs = (cl_platform_id *)alloca(sizeof(cl_platform_id) * numPlatforms);
        errNum = clGetPlatformIDs(numPlatforms, platformIDs, NULL);
        checkErr((errNum != CL_SUCCESS) ? errNum : (numPlatforms <= 0 ? -1 : CL_SUCCESS), "clGetPlatformIDs");

        for (int i = 0; i < numPlatforms; i++)
        {
            cl_device_id * deviceIDs;
            errNum = clGetDeviceIDs(platformIDs[i], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices);
            if (errNum != CL_SUCCESS && errNum != CL_DEVICE_NOT_FOUND)
            {
            checkErr(errNum, "clGetDeviceIDs");
            }
            else if (numDevices > 0)
            {
            deviceIDs = (cl_device_id *)alloca(sizeof(cl_device_id) * numDevices);
            errNum = clGetDeviceIDs(platformIDs[i], CL_DEVICE_TYPE_ALL, numDevices, &deviceIDs[0], NULL);
            checkErr(errNum, "clGetDeviceIDs");
            // break;
            }
            if (deviceIDs == NULL) {
                std::cout << "No OpenCL devices found" << std::endl;
                return;
            }
            cl_context_properties contextProperties[] = { CL_CONTEXT_PLATFORM, (cl_context_properties)platformIDs[i], 0};
            cl_context context = clCreateContext(contextProperties, numDevices, deviceIDs, &contextCallback, NULL, &errNum);
            checkErr(errNum, "clCreateContext");
            contexts.push_back(context);
            device_ids.push_back(deviceIDs);
        }
        std::cout << "Created " << contexts.size() << " contexts!" << std::endl;
        if (contexts.size() > 0)
            gpu_executor = new ContextExecutor(contexts[0], device_ids[0]);
        if (contexts.size() > 1)
            graphics_executor = new ContextExecutor(contexts[1], device_ids[1]);
    }

// private:
    ContextExecutor *gpu_executor = nullptr;
    ContextExecutor *graphics_executor = nullptr;

};