#pragma once
#include <CL/cl.h>
#include <opencv2/opencv.hpp>
#include <ocl_utils.hpp>
#include <fstream>
#include <chrono>
#include <AbstractExecutor.hpp>

class ContextExecutor : public AbstractExecutor
{
    const std::string kernel_path = "/home/ptaxom/Uni/AVP/mpicluster/Client/src/kernel.cl";
public:
    ContextExecutor(cl_context context, cl_device_id *deviceIDs) : context_(context)
    {
        std::ifstream srcFile(kernel_path);
        checkErr(srcFile.is_open() ? CL_SUCCESS : -1, "reading kernel.cl");
        std::string srcProg(std::istreambuf_iterator<char>(srcFile), (std::istreambuf_iterator<char>()));

        const char * src = srcProg.c_str();
        size_t length = srcProg.length();

        cl_int errNum;
        program_ = clCreateProgramWithSource(context, 1, &src, &length, &errNum);
        checkErr(errNum, "clCreateProgramWithSource");

        errNum = clBuildProgram(program_, 1, deviceIDs, NULL, NULL, NULL);
        if (errNum == CL_BUILD_PROGRAM_FAILURE) {
            size_t log_size = 2;
            errNum =  clGetProgramBuildInfo(program_, deviceIDs[0], CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
            checkErr(errNum, "clGetProgramBuildInfo");
            std::cout << log_size << std::endl;
            char *log = (char *) malloc(log_size);
            clGetProgramBuildInfo(program_, deviceIDs[0], CL_PROGRAM_BUILD_LOG, log_size, log, NULL);
            std::cout << std::string(log) << std::endl;
        }
        checkErr(errNum, "clBuildProgram");

        kernel_ = clCreateKernel(program_, "convolve", &errNum);
        checkErr(errNum, "clCreateKernel");

        device_id_ = deviceIDs[0];

        float kernel_template[3][3] = {
                {1, 1,  1},
                {1, -8,  1},
                {1, 1,  1}
            };
        float norm_coef = 0.;
        for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++)
                norm_coef += kernel_template[i][j];
        if (norm_coef == 0)
            norm_coef = 1.;

        for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++)
                kernel_template[i][j] /= norm_coef;

        conv_kernel_ = clCreateBuffer(context_, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                sizeof(cl_uint) * 3 * 3,
                (void *)(kernel_template), &errNum);
        checkErr(errNum, "clCreateBuffer(kernel)");

        queue_ = clCreateCommandQueue(context_, device_id_, 0, &errNum);
        checkErr(errNum, "clCreateCommandQueue");
    }

    virtual cv::Mat process(const cv::Mat &image) override
    {
        cv::Mat im2proc;
        // Make paddings
        cv::copyMakeBorder(image, im2proc, 1, 1, 1, 1, cv::BORDER_REFLECT);
        im2proc.convertTo(im2proc, CV_32FC3);
        float *input_image = im2proc.ptr<float>(0);

        float *result = process(input_image, im2proc.rows, im2proc.cols);
        
        cv::Mat output_image(im2proc.rows, im2proc.cols, CV_32FC3, result);
        cv::Rect roi(1, 1, im2proc.cols - 2, im2proc.rows - 2);
        output_image = output_image(roi);
        cv::threshold(output_image, output_image, 0, 0, cv::THRESH_TOZERO);
        cv::normalize(output_image, output_image, 0.0, 255.0, cv::NORM_MINMAX);
        output_image.convertTo(output_image, CV_8UC3);
        return output_image;
    }

    float *process(const float* input_image, const int image_height, const int image_width)
    {
        cl_int errNum;
        size_t im_size = image_height  * image_width * 3;
        cl_mem inputSignalBuffer = clCreateBuffer(context_, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                    sizeof(float) * im_size,
                    (void *)(input_image), &errNum);
        checkErr(errNum, "clCreateBuffer(input_image)");

        cl_mem outputSignalBuffer = clCreateBuffer(context_, CL_MEM_WRITE_ONLY,
                sizeof(float) * im_size,
                NULL, &errNum);
        checkErr(errNum, "clCreateBuffer(outputSignal)");
        cl_uint kernel_radius = 1;
        errNum = clSetKernelArg(kernel_, 0, sizeof(cl_mem), &inputSignalBuffer);
        errNum |= clSetKernelArg(kernel_, 1, sizeof(cl_mem), &conv_kernel_);
        errNum |= clSetKernelArg(kernel_, 2, sizeof(cl_mem), &outputSignalBuffer);
        errNum |= clSetKernelArg(kernel_, 3, sizeof(cl_uint), &image_width);
        errNum |= clSetKernelArg(kernel_, 4, sizeof(cl_uint), &kernel_radius);
        checkErr(errNum, "clSetKernelArg"); 

        size_t * global = (size_t*) malloc(sizeof(size_t)*2);
        size_t * local = (size_t*) malloc(sizeof(size_t)*2);

        
        global[0] = image_width; global[1] = image_height;
        local[0] = 1; local[1] = 1;
        
        float *result = new float[im_size];
        
        errNum = clEnqueueNDRangeKernel(queue_, kernel_, 2, NULL, global, local, 0, NULL, NULL);
        checkErr(errNum, "clEnqueueNDRangeKernel");
        errNum = clEnqueueReadBuffer(queue_, outputSignalBuffer, CL_TRUE, 0,
                    sizeof(float) * im_size,
                    result, 0, NULL, NULL);

        checkErr(errNum, "clEnqueueReadBuffer");
        clReleaseMemObject(inputSignalBuffer);
        clReleaseMemObject(outputSignalBuffer);

        return result;
    }

    


private:
    cl_context context_     = nullptr;
    
    cl_program program_     = nullptr;
    cl_kernel kernel_       = nullptr;
    cl_mem    conv_kernel_;
    cl_device_id device_id_;
    cl_command_queue queue_;

};