#pragma once
#include <AbstractExecutor.hpp>
#include <vector>
#include <ContextFactory.hpp>
#include <StreamingExecutor.hpp>
#include <pthread.h> 
#include <semaphore.h>
#include <sstream>



class ExecutorsPool;
struct Task
{
    Task(AbstractExecutor *exec, int task, ExecutorsPool *exec_pool) : executor(exec), task_id(task), pool(exec_pool)
    {}

    AbstractExecutor *executor;
    int task_id;
    ExecutorsPool *pool;
};

void *thread_payload(void *data);


class ExecutorsPool
{
public:
    ExecutorsPool(ContextFactory *context_factory, std::vector<int> tasks) : factory_(context_factory), task_ids(tasks)
    {}

    void init()
    {
        if(factory_->graphics_executor)
            executors_.push_back(factory_->graphics_executor);
        if(factory_->gpu_executor)
            executors_.push_back(factory_->gpu_executor);
        int opencl_executors = executors_.size();
        for(int i = 0; i < 5 - opencl_executors; i++)
            executors_.push_back(new StreamingExecutor());

        sem_init(&semaphore_, 0, executors_.size());
        pthread_mutex_init(&workers_mutex_, NULL);
    }

    void start_execute()
    {
        std::vector<pthread_t> threads;
        while (task_ids.size() > 0)
        {
            int task_id = task_ids.back();
            task_ids.pop_back();
            AbstractExecutor *exec = atomic_get_executor();
            Task *task = new Task(exec, task_id, this);
            std::cout << "Processing " << task_id << std::endl;
            pthread_attr_t attr;
            pthread_t thread_id;
            pthread_attr_init(&attr);
            pthread_create(&thread_id, &attr, thread_payload, reinterpret_cast<void *>(task));
            threads.push_back(thread_id);
        }
        for(pthread_t thread_id : threads)
            pthread_join(thread_id, NULL);

    }

    void atomic_add_executor(AbstractExecutor *exec)
    {
        pthread_mutex_lock(&workers_mutex_);
        executors_.push_back(exec);
        pthread_mutex_unlock(&workers_mutex_);
        sem_post(&semaphore_);
    }

    AbstractExecutor * atomic_get_executor()
    {
        sem_wait(&semaphore_); 
        pthread_mutex_lock(&workers_mutex_);
        AbstractExecutor* executor = executors_.back();
        executors_.pop_back();
        pthread_mutex_unlock(&workers_mutex_);        
        return executor;
    }
    

private:
    std::vector<AbstractExecutor*> executors_;
    std::vector<int> task_ids;
    ContextFactory *factory_;
    pthread_mutex_t queue_lock_; 
    sem_t semaphore_;
    pthread_mutex_t workers_mutex_;

};

void *thread_payload(void *data)
{
    Task *task = (Task*)data;
    // Obtain image
    cv::Mat input_data = cv::Mat::zeros(cv::Size(2280, 2280), CV_8UC3);
    // Process image
    cv::Mat output_data = task->executor->process(input_data);
    // Create image name
    std::string name;
    std::stringstream ss;
    ss << task->task_id << ".jpeg";
    ss >> name;
    // Save image
    cv::imwrite(name, output_data);
    // Release worker
    task->pool->atomic_add_executor(task->executor);
}
