#pragma once
#include <cudnn.h>
#include <dnn_utils.hpp>
#include <string>
#include <cassert>
#include <opencv2/opencv.hpp>
#include <AbstractExecutor.hpp>

class StreamingExecutor : public AbstractExecutor
{
public:
    StreamingExecutor()
    {
        cudnnCreate(&cudnn_handle_);
        init_kernel();
    }

    ~StreamingExecutor()
    {
        if (device_kernel_)
            cudaFree(device_kernel_);
        cudnnDestroyFilterDescriptor(kernel_descriptor_);
        cudnnDestroy(cudnn_handle_);
    }

    virtual cv::Mat process(const cv::Mat &image) override
    {
        cv::Mat im2proc;
        image.convertTo(im2proc, CV_32FC3);
        float *input_image = im2proc.ptr<float>(0);
        float *result = process(input_image, im2proc.rows, im2proc.cols);

        cv::Mat output_image(im2proc.rows, im2proc.cols, CV_32FC3, result);
        cv::threshold(output_image, output_image, 0, 0, cv::THRESH_TOZERO);
        cv::normalize(output_image, output_image, 0.0, 255.0, cv::NORM_MINMAX);
        output_image.convertTo(output_image, CV_8UC3);
        return output_image;
    }

    float *process(const float* input_image, const int image_height, const int image_width)
    {
        cudnnTensorDescriptor_t input_descriptor  = create_image_tensor(image_height, image_width);
        cudnnTensorDescriptor_t output_descriptor = create_image_tensor(image_height, image_width);

        cudnnConvolutionDescriptor_t convolution_descriptor;
        checkCUDNN(cudnnCreateConvolutionDescriptor(&convolution_descriptor));
        checkCUDNN(cudnnSetConvolution2dDescriptor(convolution_descriptor, 1, 1, 1, 1, 1, 1, CUDNN_CROSS_CORRELATION, CUDNN_DATA_FLOAT));

        int batch_size{0}, channels{0}, height{0}, width{0};
        checkCUDNN(cudnnGetConvolution2dForwardOutputDim(convolution_descriptor, input_descriptor, kernel_descriptor_, &batch_size, &channels, &height, &width));        

        cudnnConvolutionFwdAlgo_t convolution_algorithm;
        checkCUDNN( cudnnGetConvolutionForwardAlgorithm(cudnn_handle_, input_descriptor, kernel_descriptor_, convolution_descriptor, output_descriptor, 
                                CUDNN_CONVOLUTION_FWD_PREFER_FASTEST, 0, &convolution_algorithm));

        size_t workspace_bytes{0};
        checkCUDNN(cudnnGetConvolutionForwardWorkspaceSize(cudnn_handle_, input_descriptor, kernel_descriptor_, 
                                                            convolution_descriptor, output_descriptor, convolution_algorithm,
                                                             &workspace_bytes));

        assert(workspace_bytes > 0 && "Need memory size > 0");

        void* d_workspace{nullptr};
        cudaMalloc(&d_workspace, workspace_bytes);

        int image_bytes = 3 * height * width * sizeof(float);

        float* d_input{nullptr};
        cudaMalloc(&d_input, image_bytes);
        cudaMemcpy(d_input, input_image, image_bytes, cudaMemcpyHostToDevice);

        float* d_output{nullptr};
        cudaMalloc(&d_output, image_bytes);
        cudaMemset(d_output, 0, image_bytes);

        const float alpha = 1.f, beta = 0.0f;
        checkCUDNN(cudnnConvolutionForward(cudnn_handle_, &alpha, input_descriptor,
                                            d_input, kernel_descriptor_, device_kernel_,
                                            convolution_descriptor, convolution_algorithm,
                                            d_workspace, workspace_bytes, &beta,
                                            output_descriptor, d_output));

        float* h_output = new float[image_bytes];
        cudaMemcpy(h_output, d_output, image_bytes, cudaMemcpyDeviceToHost);

        cudaFree(d_input);
        cudaFree(d_output);
        cudaFree(d_workspace);

        cudnnDestroyTensorDescriptor(input_descriptor);
        cudnnDestroyTensorDescriptor(output_descriptor);
        cudnnDestroyConvolutionDescriptor(convolution_descriptor);

        return h_output;

    }

private:
    cudnnTensorDescriptor_t create_image_tensor(const int height, const int width)
    {
        cudnnTensorDescriptor_t tensor_descriptor;
        checkCUDNN(cudnnCreateTensorDescriptor(&tensor_descriptor));
        checkCUDNN(cudnnSetTensor4dDescriptor(tensor_descriptor, CUDNN_TENSOR_NHWC, CUDNN_DATA_FLOAT, 1, 3, height, width));
        return tensor_descriptor;
    }

    void init_kernel()
    {
          const float kernel_template[3][3] = {
                {1, 5, -10},
                {1, 5, -10},
                {1, 5, -10}
            };
            float norm_coef = 0.;
            for(int i = 0; i < 3; i++)
                for(int j = 0; j < 3; j++)
                    norm_coef += kernel_template[i][j];
            if (norm_coef == 0)
                norm_coef = 1.;

            float h_kernel[3][3][3][3] = {0.};
            for (int kernel = 0; kernel < 3; ++kernel) {
                    for (int row = 0; row < 3; ++row) {
                        for (int column = 0; column < 3; ++column) 
                            h_kernel[kernel][kernel][row][column] = kernel_template[row][column] / norm_coef;
                    }
            }

            cudaMalloc(&device_kernel_, sizeof(h_kernel));
            cudaMemcpy(device_kernel_, h_kernel, sizeof(h_kernel), cudaMemcpyHostToDevice);
    
            checkCUDNN(cudnnCreateFilterDescriptor(&kernel_descriptor_));
            checkCUDNN(cudnnSetFilter4dDescriptor(kernel_descriptor_, CUDNN_DATA_FLOAT, CUDNN_TENSOR_NCHW, 3, 3, 3, 3));
    }


private:
      cudnnHandle_t cudnn_handle_;
      float *device_kernel_ = nullptr;
      cudnnFilterDescriptor_t kernel_descriptor_;
      cudnnConvolutionFwdAlgo_t convolution_algorithm_;
};