__kernel void convolve(const __global float * const input,
            __constant float * const conv_kernel,
            __global float * const output,
            const int width,
            const int radius
)
{
    const int x = get_global_id(0) + radius;
    const int y = get_global_id(1) + radius;
    const int kernel_width = 2 * radius + 1;

    for(int channel = 0; channel < 3; channel++)
    {
        float sum = 0;
        for(int dy = -radius; dy <= radius; dy++)
            for(int dx = -radius; dx <= radius; dx++)
            {
                sum += input[3 * ((y + dy) * width + x + dx) + channel] * conv_kernel[kernel_width * (radius + dy) + radius + dx];
            }
        output[3 * (y * width + x) + channel] = sum;
    }
}