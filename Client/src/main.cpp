#include <iostream>
#include <opencv2/opencv.hpp>
#include <ocl_utils.hpp>
#include <ContextFactory.hpp>
#include <ExecutorsPool.hpp>


int main(int argc, const char* argv[]) {
  std::vector<int> tasks = {1, 2, 3, 4, 5, 6, 7};
  ContextFactory *factory = new ContextFactory();
  factory->init_contexts();
  ExecutorsPool *executors = new ExecutorsPool(factory, tasks);
  executors->init();
  executors->start_execute();
  return 0;
}
